PHP MYSQL INSTALL

https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-16-04

Apache Install

1. sudo apt-get update
2. sudo apt-get install apache2
3. sudo apache2ctl configtest
4. sudo gedit /etc/apache2/apache2.conf
ServerName server_domain_or_IP
5. sudo systemctl restart apache2
6. sudo ufw allow in "Apache Full"
Now test apache installation
Install MySQL
1. sudo apt-get install mysql-server
2. mysql_secure_installation
Follow the steps.
Install PHP
1. sudo apt-get install php libapache2-mod-php php-mcrypt php-mysql
2. sudo gedit /etc/apache2/mods-enabled/dir.conf
Move the PHP index file to the first position after the DirectoryIndex specification
1. sudo systemctl restart apache2
2. sudo apt-get install php-cli
3. sudo gedit /var/www/html/info.php



Install PHP 5.6 on Ubuntu 
1. sudo apt-get install python-software-properties
2. sudo add-apt-repository ppa:ondrej/php
3. sudo apt-get update
4. sudo apt-get install -y php5.6
Install PHP 7.1 on Ubuntu 
1. sudo apt-get install python-software-properties
2. sudo add-apt-repository ppa:ondrej/php
3. sudo apt-get update
4. sudo apt-get install -y php7.1
5. apt-get -y install php7.1-opcache php7.1-fpm php7.1 php7.1-common php7.1-gd php7.1-mysql php7.1-imap php7.1-cli php7.1-cgi php-pear php-auth php7.1-mcrypt mcrypt imagemagick libruby php7.1-curl php7.1-intl php7.1-pspell php7.1-recode php7.1-sqlite3 php7.1-tidy php7.1-xmlrpc php7.1-xsl memcached php-memcache php-imagick php-gettext php7.1-zip php7.1-mbstring

Switch Between PHP Version’s 
Apache:-

1. sudo a2dismod php5.6
2. sudo a2enmod php7.1
3. sudo service apache2 restart

CLI:-

1. update-alternatives --set php /usr/bin/php7.1



Install phpMyAdmin
1. sudo apt-get update
2. sudo apt-get install phpmyadmin php-mbstring php-gettext
3. sudo phpenmod mcrypt
4. sudo phpenmod mbstring
5. sudo systemctl restart apache2
Browse https://domain_name_or_IP/phpmyadmin
Install Chrome
1. sudo apt-get install libxss1 libappindicator1 libindicator7
2. wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
3. sudo dpkg -i google-chrome*.deb

Install Sublime

For Sublime-Text-3:
1. sudo add-apt-repository ppa:webupd8team/sublime-text-3
2. sudo apt-get update
3. sudo apt-get install sublime-text-installer

Install GIT
1. sudo apt install git

Install Composer

1. sudo apt-get update
2. sudo apt-get install curl
3. sudo curl -s https://getcomposer.org/installer | php
4. sudo mv composer.phar /usr/local/bin/composer
